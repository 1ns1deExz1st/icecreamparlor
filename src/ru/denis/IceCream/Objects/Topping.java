package ru.denis.IceCream.Objects;

import ru.denis.IceCream.Decorators.Decorator;

public interface Topping  {
    void addSprinkles();
}
