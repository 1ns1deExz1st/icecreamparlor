package ru.denis.IceCream.Objects;

import ru.denis.IceCream.Decorators.Decorator;

public class Syrup implements Topping {

    @Override
    public void addSprinkles() {
        System.out.println("Добавлен сироп.");
    }
}
