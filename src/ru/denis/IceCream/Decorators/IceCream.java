package ru.denis.IceCream.Decorators;

import ru.denis.IceCream.Objects.Topping;

public class IceCream extends Decorator {
    IceCream(Topping topping) {
        super(topping);
    }

    @Override
    public void oF() {
        System.out.println("К мороженному ");
    }
}
