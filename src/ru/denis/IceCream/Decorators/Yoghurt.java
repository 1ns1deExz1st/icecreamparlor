package ru.denis.IceCream.Decorators;

import ru.denis.IceCream.Objects.Topping;

public class Yoghurt extends Decorator {
    Yoghurt(Topping topping) {
        super(topping);
    }

    @Override
    public void oF() {
        System.out.println("К йогурту ");
    }
}
