package ru.denis.IceCream.Decorators;

import ru.denis.IceCream.Objects.Topping;

public abstract class Decorator implements Topping {
    private Topping topping;

    Decorator(Topping topping) {
        this.topping = topping;
    }
    public abstract void oF();

    @Override
    public void addSprinkles() {
        topping.addSprinkles();
        oF();
    }
}